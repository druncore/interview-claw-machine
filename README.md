# Grandiosa Klomaskin

Skapa en enkel men snygg klomaskin från bifogade assets.


## Funktioner

Användaren ska kunna styra klon vänster/höger med piltangenterna.
Vid klick eller mellanslag/enter åker klon ner bakom bordsskivan.
Efter en kort stund kommer klon upp med en pizzakartong.

Se bifogad video för inspiration.


## Assets

Bakgrund, pizzahög och bordsskivan är alla 1920x1080 så lättast är nog att göra scenen i den storleken.
Klons delar och den ensamma pizzakartongen är större än vad de egentligen behöver vara men det skalas enkelt i css.
Vi ser gärna att klon animeras på ett passande sätt.
Det behöver inte fungera perfekt på mobil, speciellt då tangentstyrning är svårt att få till ;)


### Teknik

Vi föredrar att du använder Vue som grund i projektet.
Använd den teknik du vill i övrigt, utom att spela upp det som video då ;), men det ska kunna köras i senaste stabila versionen av Chrome.

Håll det enkelt, fokusera på visuell känsla men lägg inte för mycket tid på det hela.

